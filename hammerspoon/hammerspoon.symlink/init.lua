local MEH = {"shift", "alt", "ctrl"}
local HYPER = {"cmd", "alt", "ctrl", "shift"}

local focusedWindow
local focusedWindowFrame
local screenFrame

hs.grid.MARGINX = 0
hs.grid.MARGINY = 0



--------------------------------------------------------------------------------
-- Hotkeys

hs.hotkey.bind(MEH, "D", function() hs.grid.pushWindowNextScreen() end)
hs.hotkey.bind(MEH, "F", function() operateFrame(maximize) end)
hs.hotkey.bind(MEH, "B", function() operateFrame(resizeAndCenter, 0.7) end)
hs.hotkey.bind(MEH, "N", function() operateFrame(resizeAndCenter, 0.6) end)
hs.hotkey.bind(MEH, "M", function() operateFrame(resizeAndCenter, 0.5) end)
hs.hotkey.bind(MEH, "K", function() operateFrame(adjustToTopHalf) end)
hs.hotkey.bind(MEH, "J", function() operateFrame(adjustToBottomHalf) end)
hs.hotkey.bind(MEH, "H", function() operateFrame(adjustToLeftHalf) end)
hs.hotkey.bind(MEH, "L", function() operateFrame(adjustToRightHalf) end)


--------------------------------------------------------------------------------
-- High level functions

function operateFrame(operation, optionalRatio1, optionalRatio2)
    initFrames()
    operation(optionalRatio1, optionalRatio2)
    applyFrameChanges()
end

function maximize()
    maximizeWidth()
    maximizeHeight()
end

function resizeAndCenter(ratio)
    resizeWidth(ratio)
    resizeHeight(ratio)
    centerHorizontally()
    centerVertically()
end

function adjustToRightHalf()
    maximizeHeight()
    resizeWidth(0.5)
    moveCompletelyToRight()
end

function adjustToLeftHalf()
    maximizeHeight()
    resizeWidth(0.5)
    moveCompletelyToLeft()
end

function adjustToTopHalf()
    maximizeWidth()
    resizeHeight(0.5)
    moveCompletelyToTop()
end

function adjustToBottomHalf()
    maximizeWidth()
    resizeHeight(0.5)
    moveCompletelyToBottom()
end



--------------------------------------------------------------------------------
-- Low level functions

function initFrames()
    focusedWindow = hs.window.focusedWindow()
    focusedWindowFrame = focusedWindow:frame()
    screenFrame = focusedWindow:screen():frame()
end

function applyFrameChanges()
    focusedWindow:setFrame(focusedWindowFrame)
end

function maximizeWidth()
    focusedWindowFrame.w = screenFrame.w
    focusedWindowFrame.x = screenFrame.x
end

function maximizeHeight()
    focusedWindowFrame.h = screenFrame.h
    focusedWindowFrame.y = screenFrame.y
end

function resizeWidth(ratio)
    focusedWindowFrame.w = screenFrame.w * ratio
end

function resizeHeight(ratio)
    focusedWindowFrame.h = screenFrame.h * ratio
end

function centerHorizontally()
    focusedWindowFrame.x = screenFrame.x + (screenFrame.w - focusedWindowFrame.w) / 2
end

function centerVertically()
    focusedWindowFrame.y = screenFrame.y + (screenFrame.h - focusedWindowFrame.h) / 2
end

function increaseWidthAtRight(ratio)
    focusedWindowFrame.w = focusedWindowFrame.w + screenFrame.w * ratio
end

function increaseWidthAtLeft(ratio)
    local increment = math.min(focusedWindowFrame.x, screenFrame.w * ratio)
    focusedWindowFrame.x = focusedWindowFrame.x - increment
    focusedWindowFrame.w = focusedWindowFrame.w + increment
end

function increaseHeightAtBottom(ratio)
    focusedWindowFrame.h = focusedWindowFrame.h + screenFrame.w * ratio
end

function increaseHeightAtTop(ratio)
    local increment = math.min(focusedWindowFrame.y - getMinY(), screenFrame.h * ratio)
    focusedWindowFrame.h = focusedWindowFrame.h + increment
    focusedWindowFrame.y = focusedWindowFrame.y - increment
end

function decreaseWidthAtRight(ratio)
    focusedWindowFrame.w = focusedWindowFrame.w - screenFrame.w * ratio
end

function decreaseWidthAtLeft(ratio)
    local decrement = screenFrame.w * ratio
    if (decrement < focusedWindowFrame.w) then
        focusedWindowFrame.x = focusedWindowFrame.x + decrement
        focusedWindowFrame.w = focusedWindowFrame.w - decrement
    end
end

function decreaseWidthAtTop(ratio)
    local decrement = screenFrame.h * ratio
    if (decrement < focusedWindowFrame.h) then
        focusedWindowFrame.y = focusedWindowFrame.y + decrement
        focusedWindowFrame.h = focusedWindowFrame.h - decrement
    end
end

function decreaseWidthAtBottom(ratio)
    focusedWindowFrame.h = focusedWindowFrame.h - screenFrame.h * ratio
end

function moveCompletelyToLeft()
    focusedWindowFrame.x = screenFrame.x
end

function moveCompletelyToRight()
    focusedWindowFrame.x = screenFrame.x + (screenFrame.w - focusedWindowFrame.w)
end

function moveCompletelyToTop()
    focusedWindowFrame.y = screenFrame.y
end

function moveCompletelyToBottom()
    focusedWindowFrame.y = screenFrame.y + (screenFrame.h - focusedWindowFrame.h)
end

function moveToTopKeepingVisibility(ratio)
    focusedWindowFrame.y = focusedWindowFrame.y - screenFrame.h * ratio
end

function moveToBottomKeepingVisibility(ratio)
    local newY = focusedWindowFrame.y + screenFrame.h * ratio
    focusedWindowFrame.y = math.min(newY, getMinY() + screenFrame.h - focusedWindowFrame.h)
end

function moveToLeftKeepingVisibility(ratio)
    local newX = focusedWindowFrame.x - screenFrame.w * ratio
    focusedWindowFrame.x = math.max(0, newX)
end

function moveToRightKeepingVisibility(ratio)
    local newX = focusedWindowFrame.x + screenFrame.w * ratio
    focusedWindowFrame.x = math.min(newX, screenFrame.w - focusedWindowFrame.w)
end

-- Min coordinate Y isn't 0 because of the Mac's menu bar.
function getMinY()
    return hs.window.focusedWindow():screen():frame().y
end



--------------------------------------------------------------------------------
-- Reload config after saving

function reload_config(files)
    hs.reload()
end
hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", reload_config):start()
hs.alert.show("[Hammerspoon] Config reloaded")
